const express = require("express");
const mongoose = require("mongoose");

// Server Preperation
const app = express();
const port = 3001;

mongoose.set('strictQuery', true)


// [SECTIOn] - MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin1234@cluster0.n5pfk4s.mongodb.net/S35?retryWrites=true&w=majority",{
	useNewUrlParser : true,
	useUnifiedTopology: true,
});//give the password; inbetween of "/" and "?" put the database

// set notification for connection success or failure

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"));

// [Section] - Schemas
// Schemas determine the structure of the documents to be written in the database
// Schemas act as blueprints to our data
// Use the Schema() constructor of the Mongoose module to create a new Schema object
// The "new" keyword creates a new Schema

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}

});


const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: [true, "Username is required"]
	},
	password: {
		type: String,
		required: [true, "password is required"]
	}
})

// [Section] -Model (lagin magkasam ang schema at model (siblings))
const Task = mongoose.model("Task", taskSchema) // must be capitalize the first letter of the variable)
const User = mongoose.model("User", userSchema)


// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));//lahat ng input ng user in any kind ay convert para mabasa ng server
app.listen(port, () => console.log(`Server running at port ${port}`));

// Creating a New Task
// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
- If the task already exists in the database, we return an error
- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/


app.post("/task", (req,res) => {
	//mag hahanap ng isang data sa db : find()pag lahat
	Task.findOne({name: req.body.name}, (err, result) =>{
	

		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		}else{
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, saveTask) =>{
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New Task Created");
				}
			});
		}
	});

});

// Getting all task

app.get("/tasks", (req,res) =>{
	Task.find({}, (err,result) => {
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json({
				data: result
			})
		}
	})
})

// [SECTION] ACTIVITY //////////////////////////


app.post('/user', (req, res) => {
	User.findOne({username: req.body.username}, (err, result) =>{
	

		if(result != null && result.username == req.body.username){
			return res.send("Duplicate user found");
		}else{
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, saveError) =>{
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New User Created");
				}
			});
		}
	});
})


app.get("/users", (req,res) =>{
	User.find({}, (err,result) => {
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json({
				data: result
			})
		}
	})
})